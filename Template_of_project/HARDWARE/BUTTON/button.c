#include "button.h"
#include "stm32f4xx.h"
#include "delay.h"
void BUTTON_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOE,ENABLE);//初始化时钟使能GPIO的A和E口
	
	//初始化，使能
	//GOIOE的一端下拉到地，没有按下时候另一端是高电平，所以是上拉GPIO_PuPd_UP
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4; 
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN;//普通输入模式
	//GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;//上拉
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOE,&GPIO_InitStructure);//初始化GPIOE的1,2,3,
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;//WK_UP对应的引脚
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_DOWN;//下拉
	GPIO_Init(GPIOF,&GPIO_InitStructure);
	//GPIO_SetBits(GPIOE,GPIO_Pin_4);
	
}
u8 BUTTON_Scan(u8 mode)//返回值是mode，数据类型uint8形式，传递给main函数，变成button的值
//对按键当前状态进行扫描，加入mode。用来选择连续按键和不连续按键的模式
{
	static u8 button_up=1;//开关松开标志
	/*
	下一次调用button_up时，button_up的值是上一次的返回值，不一定是1
	if(button_up && button按下)，如果上一次的状态是没有按下，并且这一次按下，
	
	(排除长按导致两次扫描到的结果都是开关按下)
	
	则进行后面的操作
	*/
	if(mode) 
		button_up=1;//mode=1,支持连续;mode=0，不支持连续
	
	if(button_up&&(BUTTON0==0|BUTTON1==0|BUTTON2==0))//上一拍没有开关按下，这一拍有某个开关按下
		//如果上一拍没有开关按下，这一拍有某个开关按下
	 {
		 delay_ms(20);
		 button_up=0;//确认已经按下某个开关，这个button_up的值储存下来用到下一拍的判断中
		 if     (BUTTON0==0)   return 1;//如果是button0开关按下，对
		 else if(BUTTON1==0)   return 2;
		 else if(BUTTON2==0)   return 3;
		 else if(WK_UP==1)     return 4; 
		 //上面的返回值赋给button，在main函数中用到
	 }
	else if(BUTTON0==1&&BUTTON1==1&&BUTTON2==1&&WK_UP==1)//确认没有开关按下
		//如果这几个开关都是没按下的状态
	  //则button_up=1，确认开关没按下
		button_up=1;//确认没有开关按下，这个button_up的值储存下来用到下一拍的判断中
	return 0;
}
