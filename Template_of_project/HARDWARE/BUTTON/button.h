#ifndef __BUTTON_H
#define __BUTTON_H
#include "sys.h"


//下面通过直接操作函数库的方式读取IO口
#define BUTTON0 GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_4)
#define BUTTON1 GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_3)
#define BUTTON2 GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_2)
#define WK_UP GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)
//#define BUTTON0 HIL_GPIO_ReadPin(GPIOE,GPIO_PIN_4)


#define BUTTON0_PRES 1
#define BUTTON1_PRES 2
#define BUTTON2_PRES 3
#define WKUP_PRES 4

void BUTTON_Init(void);//初始化
u8 BUTTON_Scan(u8 mode);//扫描函数
#endif
