#include "stm32f4xx.h"
#include "usart.h"
#include "led.h"
#include "beep.h"
#include "delay.h"
#include "button.h"

int main(void)
{
	u8 button;//接收button.c的返回值（mode，uint8类型）赋给u8类型的button
	delay_init(168);//这里168是默认的
	LED_Init();//调用LED_Init,在led.c里面
	BEEP_Init();//初始化蜂鸣器
	BUTTON_Init();
	LED0=0;//led0是红灯，0表示亮
	
	while(1)//重复执行后面的代码
	{
		button=BUTTON_Scan(1);//得到按键编号
		//这里的0就是button.c中的mode值，mode=1,支持连续;mode=0，不支持连续
		if(button)//button如果>0则执行后续操作，如果=0不执行后续操作
		{
			switch(button)
			{
				case 1:	//button的值=1
		        //GPIO_ResetBits(GPIOF,GPIO_Pin_8);//蜂鸣器响
		        LED0=1;//红灯灭
		        LED1=0;//绿灯亮
				GPIO_SetBits(GPIOF,GPIO_Pin_8);//蜂鸣器响
		        break;
				case 2://1号按键
					  LED0=0;
		        LED1=1;
				    
		        GPIO_SetBits(GPIOF,GPIO_Pin_8);//蜂鸣器响
				    break;
				case 3:	//3号按键
		       GPIO_ResetBits(GPIOF,GPIO_Pin_8);
		        break;
				case 4:	//WK_UP按键	        
				    GPIO_SetBits(GPIOF,GPIO_Pin_8);//蜂鸣器响
		        break;
			}
		}
		else delay_ms(20);
	}
}
